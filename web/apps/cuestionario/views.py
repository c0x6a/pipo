# -*- coding: utf-8 -*-
"""
Vistas de la aplicación cuestionario
"""
# 3rd part imports
import datetime
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Q
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils import timezone
from mail_templated import send_mail

# local app imports
from apps.cuestionario.models import Cuestionario, Pregunta, Categoria
from apps.cuestionario.models import cuestionarioAlumno, Alternativa
from apps.cuestionario.models import cuestionarioAlumnoDetalles
from apps.cuestionario.models import Resultado
from apps.personas.models import Alumno
from libs.utils import grafico_reporte


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='alumno').count() == 1,
                  login_url='/')
def cuestionario_home(request):
    """
    Página de inicio de cuestionario
    """
    # Listar las cuestionarios del sistema, mostrar solamente las que se pueden
    # llenar en la fecha actual
    cuestionarios = Cuestionario.objects.filter(
        fecha_inicio__lte=datetime.datetime.now().date(),
        fecha_final__gte=datetime.datetime.now().date()
    )
    data = {
        'cuestionarios': cuestionarios
    }
    return render_to_response('cuestionario_home.html', data)


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='alumno').count() == 1,
                  login_url='/')
def mostrar_cuestionario(request, id_cuestionario):
    """
    Mostra la cuestionario para ser llenada
    :param id_cuestionario:  ID de la cuestionario a ser mostrada
    """
    # Recuperar el alumno que contestó la cuestionario, mediante su usuario
    usuario = request.user
    alumno = Alumno.objects.get(codigo=usuario)
    # Select * from Alumno where codigo=usuario

    # Ver la cuestionario a ser llenada
    cuestionario = Cuestionario.objects.get(id=id_cuestionario)
    # Select * from cuestionario_cuestionario where id = id_cuestionario

    # Ver si el alumno ya resolvió la cuestionario anteriormente, de ser así
    # mostrar los resultados obtenidos
    try:
        # cuestionario contestada con anterioridad
        cuestionario_llena = cuestionarioAlumno.objects.get(
            cuestionario=cuestionario, alumno=alumno)
        # Mostrar resultados
        return HttpResponseRedirect(
            '/cuestionario/resultado/%s/' % cuestionario_llena.cuestionario_id)

    except cuestionarioAlumno.DoesNotExist:
        # cuestionario no ha sido contestada con anterioridad

        # Obtener las categorías de la cuestionario
        categorias = Categoria.objects.filter(cuestionario=cuestionario)
        # Teorico, Prgamatico, Anailito, Reflexivo

        # Obtener las preguntas de la cuestionario
        preguntas = Pregunta.objects.filter(
            Q(categoria_id__in=[categoria.id for categoria in categorias]))
        # store procedure listar_preguntas
        # Select * from preguntas where categoria in (Select * from Categoria)

        # Alternativas del cuestionario
        alternativas = Alternativa.objects.filter(cuestionario=cuestionario)

        data = {
            'cuestionario': cuestionario,
            'preguntas': preguntas,
            'alternativas': alternativas
        }
        return render_to_response(
            'cuestionario_llenar.html', data,
            context_instance=RequestContext(request))


def guardar_preguntas(cuestionario_llenada, respuestas):
    # Guardar las respuestas en la BD
    """
    Guardar las preguntas en la BD
    :param cuestionario_llenada: cuestionario llenada
    :param respuestas:  Preguntas contestadas como «Sí»
    """
    for pregunta, respuesta in respuestas.iteritems():
        try:
            detalle = cuestionarioAlumnoDetalles.objects.get(
                cuestionario_alumno=cuestionario_llenada, pregunta_id=pregunta)
        except cuestionarioAlumnoDetalles.DoesNotExist:
            detalle = cuestionarioAlumnoDetalles()
            detalle.cuestionario_alumno = cuestionario_llenada
            detalle.pregunta_id = pregunta
            detalle.respuesta = int(respuesta)
            detalle.save()


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='alumno').count() == 1,
                  login_url='/')
def guardar_resultados(request):
    """
    Mostrar resultados de la cuestionario
    """
    if request.method == 'POST':
        # Recuperar la cuestionario que se llenó
        id_cuestionario = request.POST.get('id_cuestionario')
        cuestionario = Cuestionario.objects.get(id=id_cuestionario)
        # Recuperar el alumno que contestó la cuestionario, mediante su usuario
        usuario = request.user
        alumno = Alumno.objects.get(codigo=usuario)
        # Almacenar el alumno que llenó la cuestionario
        try:
            cuestionario_llena = cuestionarioAlumno.objects.get(
                alumno=alumno, cuestionario=cuestionario)
        except cuestionarioAlumno.DoesNotExist or \
                cuestionarioAlumno.MultipleObjectsReturned:
            cuestionario_llena = cuestionarioAlumno(
                alumno=alumno,
                cuestionario=cuestionario,
                fecha=timezone.now().date(),
                hora=timezone.now().time()
            )
            cuestionario_llena.save()

        # Recuperar los valores de las alternativas seleccionadas
        respuestas = {}
        for pregunta in request.POST:
            if str(pregunta).isdigit():
                respuestas[pregunta] = request.POST.get(pregunta)

        # Guardar las preguntas en la BD
        guardar_preguntas(cuestionario_llena, respuestas)

        # Mostrar los resultados
        return HttpResponseRedirect(
            '/cuestionario/resultado/%d/' % cuestionario_llena.cuestionario.id)

    raise Http404


def calcular_resultados(cuestionario_llena):
    """
    Calcular los resultados de la cuestionario llena
    :param cuestionario_llena: cuestionario a calcular los resultados
    """
    # Recuperar las respuestas de la cuestionario contestada
    detalles = cuestionarioAlumnoDetalles.objects.filter(
        cuestionario_alumno=cuestionario_llena)
    # Diccionario que almacenará la cantidad de respuestas por categoría
    respuestas = {}
    # Obtener las categorías de la cuestionario
    categorias = Categoria.objects.filter(
        cuestionario=cuestionario_llena.cuestionario)

    for categoria in categorias:
        this_detalles = detalles.filter(pregunta__categoria=categoria)
        respuestas[categoria] = sum(
            [detalle.respuesta for detalle in this_detalles])

    for categoria, cantidad in respuestas.iteritems():
        resultado = Resultado()
        resultado.cuestionario_llena = cuestionario_llena
        resultado.categoria = categoria
        resultado.cantidad = cantidad
        resultado.save()

    return Resultado.objects.filter(cuestionario_llena=cuestionario_llena)


def enviar_correo(data, request):
    """
    Enviar resultados por correo
    """
    try:
        if request.method == 'POST':
            email = request.POST.get('email')

            # send_mail(
            #     'Resultados PIPO', 'Estos son sus resultados.',
            #     'pipo@cinthyaroxana.com',
            #     ['roxy.cin@gmail.com'], fail_silently=False)

            send_mail('resultado_mail.html', data,
                      'pipo@cinthyaroxana.com',
                      [email])
            data['mensaje'] = 'Resultados enviados al correo.'
    except Exception as e:
        print e
        data['mensaje_err'] = 'Hubo un error al enviar el mensaje.'


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(
    Q(name='docente') | Q(name='alumno')).count() >= 1, login_url='/')
def ver_resultados(request, id_cuestionario):
    """
    Ver resultados de la cuestionario
    :param id_cuestionario:  ID de la cuestionario contestada para mostrar
                             resultados
    """
    # Recuperar el alumno que contestó la cuestionario, mediante su usuario
    usuario = request.user
    alumno = Alumno.objects.get(codigo=usuario)

    # Recuperar cuestionario contestada
    cuestionario_llena = cuestionarioAlumno.objects.get(
        cuestionario_id=id_cuestionario,
        alumno=alumno)
    # Mostrar los resultados
    resultados = Resultado.objects.filter(cuestionario_llena=cuestionario_llena)

    if not resultados.count():
        # La cuestionario todavía no fue procesada
        resultados = calcular_resultados(cuestionario_llena)

    grafico_columna = grafico_reporte(resultados, None, 'column')
    # Calcular resultados
    data = {
        'cuestionario_llena': cuestionario_llena,
        'resultados': resultados,
        'grafico_columna': grafico_columna
    }

    # Enviar por correo
    enviar_correo(data, request)

    return render_to_response('cuestionario_resultado.html', data,
                              context_instance=RequestContext(request))


def ver_tutor(request, nombre_tutor):
    """
    Ver tutor
    """
    return render_to_response('%s.html' % nombre_tutor)


def ver_tutor_docente(request, nombre_tutor):
    return render_to_response('docente%s.html' % nombre_tutor)