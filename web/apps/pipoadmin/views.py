# -*- coding: UTF-8 -*-
"""
Vistas de la aplicación de administración
"""
# sys imports
# 3rd part import
import re
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, Http404
from django.template.context import RequestContext
from django.db.models import Q, Count
# Local imports
from apps.alumno.forms import AlumnoFormAdmin, AlumnoFormAgregar
from apps.docente.forms import DocenteFormAdmin, DocenteFormAgregar
from apps.personas.models import Alumno, Docente, Curso, Matricula
from apps.pipoadmin.forms import CargarArchivoForm, SemestresForm
from apps.cuestionario.forms import CuestionarioForm, PreguntaForm
from apps.cuestionario.forms import AlternativaForm, cuestionarioSeleccionarForm
from apps.cuestionario.forms import TextoAyudaForm, CategoriaForm
from apps.cuestionario.models import Cuestionario, Categoria, Pregunta
from apps.cuestionario.models import TextoAyuda, Alternativa
from apps.personas.forms import CursoFormAdmin, CursoFormAgregar, MatriculaForm
from libs.utils import importar_alumno_archivo, importar_docente_archivo
from libs.utils import importar_cursos_archivo, importar_matricula_archivo


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def admin_home(request):
    """
    Página de inicio de la aplicación de administración
    """
    return render_to_response('admin_home.html')


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def alumnos_listar(request):
    """
    Lista de alumnos en el sistema
    """
    alumnos = None
    filtro = request.GET.get('filtro')
    if filtro:
        if re.match('\d+', filtro):
            try:
                alumnos = Alumno.objects.filter(codigo__contains=filtro)
            except Alumno.DoesNotExist:
                alumnos = []
        elif re.match('\w+', filtro):
            alumnos = Alumno.objects.filter(
                apellido_paterno__icontains=filtro).order_by('apellido_paterno')
    else:
        alumnos = Alumno.objects.all().order_by('apellido_paterno')

    data = {
        'alumnos': alumnos
    }
    return render_to_response('alumnos_listar.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def alumnos_agregar(request):
    """
    Agregar alumnos al sistema
    """
    try:
        alumno_form = AlumnoFormAgregar()
        mensaje_ok = mensaje_err = ''

        if request.method == 'POST':
            alumno_form = AlumnoFormAgregar(request.POST)
            if alumno_form.is_valid():
                alumno_form.save()
                mensaje_ok = 'Datos guardados correctamente'
            else:
                mensaje_err = 'Error al guardar los datos'

        data = {
            'alumno_form': alumno_form,
            'mensaje_ok': mensaje_ok,
            'mensaje_err': mensaje_err,
        }

        return render_to_response('alumno_form.html', data,
                                  context_instance=RequestContext(request))
    except Exception as e:
        print e
        raise Http404


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def alumnos_importar(request):
    """
    Importar alumnos desde un archivo externo en formato .xlsx
    """
    mensaje = None
    # Cargar formulario de subida de archivo
    archivo_form = CargarArchivoForm()
    # Ver si se mandó con metodo POST
    if request.method == "POST":
        archivo_form = CargarArchivoForm(request.POST, request.FILES)
        # Ver que es formulario tenga datos válidos
        if archivo_form.is_valid():
            alumnos_importados = importar_alumno_archivo(
                request.FILES['archivo'])
            # mensaje = 'Archivo importado correctamente'
            # class_well = 'success text-success text-success'

            if not alumnos_importados:
                mensaje = '''El archivo no contiene datos válidos, o ya
                fueron importados todos los datos anteriormente'''
            else:
                data = {
                    'alumnos': alumnos_importados
                }
                return render_to_response(
                    'alumnos_listar.html', data,
                    context_instance=RequestContext(request))

    data = {
        'formulario': archivo_form,
        'mensaje': mensaje,
        'titulo_importar': 'datos de alumnos',
    }

    return render_to_response('archivo_importar.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def alumno_editar(request, codigo_alumno):
    """
    Editar datos de un alumnos
    """
    try:
        alumno = Alumno.objects.get(codigo=codigo_alumno)
        alumno_form = AlumnoFormAdmin(instance=alumno)
        mensaje_ok = mensaje_err = ''

        if request.method == 'POST':
            alumno_form = AlumnoFormAdmin(request.POST, instance=alumno)
            if alumno_form.is_valid():
                alumno_temp = alumno_form.save(commit=False)
                # Evitar cambio de código
                alumno_temp.codigo = alumno.codigo
                alumno_temp.save()
                mensaje_ok = 'Datos actualizados correctamente'
            else:
                mensaje_err = 'Error al acutalizar los datos'

        data = {
            'alumno_form': alumno_form,
            'alumno': alumno,
            'mensaje_ok': mensaje_ok,
            'mensaje_err': mensaje_err,
        }

        return render_to_response('alumno_form.html', data,
                                  context_instance=RequestContext(request))
    except Exception as e:
        print e
        raise Http404


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def alumno_eliminar(request):
    """
    Eliminar alumno
    """
    try:
        codigo_alumno = request.POST.get('codigo_alumno')
        alumno = Alumno.objects.get(codigo=codigo_alumno)
        usuario = User.objects.get(username=codigo_alumno)
        alumno.delete()
        usuario.delete()
    except Exception as e:
        print e
        raise Http404

    return HttpResponseRedirect('/admin/alumnos/listar/')


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def docente_agregar(request):
    """
    Agregar docente al sistema
    """
    try:
        docente_form = DocenteFormAgregar()
        mensaje_ok = mensaje_err = ''

        if request.method == 'POST':
            docente_form = DocenteFormAgregar(request.POST)
            if docente_form.is_valid():
                docente_form.save()
                mensaje_ok = 'Datos guardados correctamente'
            else:
                mensaje_err = 'Error al guardar los datos'

        data = {
            'docente_form': docente_form,
            'mensaje_ok': mensaje_ok,
            'mensaje_err': mensaje_err,
        }

        return render_to_response('docente_form.html', data,
                                  context_instance=RequestContext(request))
    except Exception as e:
        print e
        raise Http404


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def docentes_importar(request):
    """
    Importar alumnos desde un archivo externo en formato .xlsx
    """
    mensaje = None
    archivo_form = CargarArchivoForm()
    if request.method == "POST":
        archivo_form = CargarArchivoForm(request.POST, request.FILES)
        if archivo_form.is_valid():
            docentes_importados = importar_docente_archivo(
                request.FILES['archivo'])
            # mensaje = 'Archivo importado correctamente'
            # class_well = 'success text-success text-success'

            if not docentes_importados:
                mensaje = '''El archivo no contiene datos válidos, o ya
                fueron importados todos los datos anteriormente'''
            else:
                data = {
                    'docentes': docentes_importados
                }
                return render_to_response(
                    'docentes_listar.html', data,
                    context_instance=RequestContext(request))

    data = {
        'formulario': archivo_form,
        'mensaje': mensaje,
        'titulo_importar': 'datos de docentes',
    }

    return render_to_response('archivo_importar.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def docente_editar(request, codigo_docente):
    """
    Editar datos de un alumnos
    """
    try:
        docente = Docente.objects.get(codigo=codigo_docente)
        docente_form = DocenteFormAdmin(instance=docente)
        mensaje_ok = mensaje_err = ''

        if request.method == 'POST':
            docente_form = DocenteFormAdmin(request.POST, instance=docente)
            if docente_form.is_valid():
                docente_temp = docente_form.save(commit=False)
                # Evitar cambio de código
                docente_temp.codigo = docente.codigo
                docente_temp.save()
                mensaje_ok = 'Datos actualizados correctamente'
            else:
                mensaje_err = 'Error al acutalizar los datos'

        data = {
            'docente_form': docente_form,
            'docente': docente,
            'mensaje_ok': mensaje_ok,
            'mensaje_err': mensaje_err,
        }

        return render_to_response('docente_form.html', data,
                                  context_instance=RequestContext(request))
    except Exception as e:
        print e
        raise Http404


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def docente_eliminar(request):
    """
    Eliminar docente
    """
    try:
        codigo_docente = request.POST.get('codigo_docente')
        docente = Docente.objects.get(codigo=codigo_docente)
        usuario = User.objects.get(username=codigo_docente)
        docente.delete()
        usuario.delete()
    except Exception as e:
        print e
        raise Http404

    return HttpResponseRedirect('/admin/docentes/listar/')


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def docentes_listar(request):
    """
    Lista de docentes en el sistema
    """
    docentes = None
    filtro = request.GET.get('filtro')
    if filtro:
        if re.match('\d+', filtro):
            try:
                docentes = Docente.objects.filter(codigo__contains=filtro)
            except Docente.DoesNotExist:
                docentes = []
        elif re.match('\w+', filtro):
            docentes = Docente.objects.filter(
                apellido_paterno__icontains=filtro).order_by('apellido_paterno')
    else:
        docentes = Docente.objects.all().order_by('apellido_paterno')

    data = {
        'docentes': docentes
    }
    return render_to_response('docentes_listar.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def cursos_listar(request):
    """
    Listar cursos en el sistema
    """
    cursos = None
    filtro = request.GET.get('filtro')
    if filtro:
        if re.match('[A-Z]{0,2}\d{0,3}', filtro):
            try:
                cursos = Curso.objects.filter(codigo__icontains=filtro)
            except Curso.DoesNotExist:
                cursos = []
        elif re.match('\w+', filtro):
            cursos = Curso.objects.filter(
                nombre__icontains=filtro).order_by('nombre')
    else:
        cursos = Curso.objects.all().order_by('nombre')

    data = {
        'cursos': cursos
    }
    return render_to_response('cursos_listar.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def curso_agregar(request):
    """
    Agregar curso al sistema
    """
    try:
        curso_form = CursoFormAgregar()
        mensaje_ok = mensaje_err = ''

        if request.method == 'POST':
            curso_form = CursoFormAgregar(request.POST)
            if curso_form.is_valid():
                curso_form.save()
                mensaje_ok = 'Datos guardados correctamente'
            else:
                mensaje_err = 'Error al guardar los datos'

        data = {
            'curso_form': curso_form,
            'mensaje_ok': mensaje_ok,
            'mensaje_err': mensaje_err,
        }

        return render_to_response('cursos_form.html', data,
                                  context_instance=RequestContext(request))
    except Exception as e:
        print e
        raise Http404


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def cursos_importar(request):
    """
    Importar cursos desde un archivo externo en formato .xlsx
    """
    mensaje = None
    archivo_form = CargarArchivoForm()
    if request.method == "POST":
        archivo_form = CargarArchivoForm(request.POST, request.FILES)
        if archivo_form.is_valid():
            cursos_importados = importar_cursos_archivo(
                request.FILES['archivo'])
            # mensaje = 'Archivo importado correctamente'
            # class_well = 'success text-success text-success'

            if not cursos_importados:
                mensaje = '''El archivo no contiene datos válidos, o ya
                fueron importados todos los datos anteriormente'''
            else:
                data = {
                    'cursos': cursos_importados
                }
                return render_to_response(
                    'cursos_listar.html', data,
                    context_instance=RequestContext(request))

    data = {
        'formulario': archivo_form,
        'mensaje': mensaje,
        'titulo_importar': 'cursos',
    }

    return render_to_response('archivo_importar.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def cursos_editar(request, codigo_curso):
    """
    Editar Cursos
    """
    try:
        curso = Curso.objects.get(codigo=codigo_curso)
        curso_form = CursoFormAdmin(instance=curso)
        mensaje_ok = mensaje_err = ''

        if request.method == 'POST':
            curso_form = CursoFormAdmin(request.POST, instance=curso)
            if curso_form.is_valid():
                curso_tmp = curso_form.save(commit=False)
                # Evitar cambio de código
                curso_tmp.codigo = curso.codigo
                curso_tmp.save()
                mensaje_ok = 'Datos actualizados correctamente'
            else:
                mensaje_err = 'Error al acutalizar los datos'

        data = {
            'curso_form': curso_form,
            'curso': curso,
            'mensaje_ok': mensaje_ok,
            'mensaje_err': mensaje_err,
        }

        return render_to_response('cursos_form.html', data,
                                  context_instance=RequestContext(request))
    except Exception as e:
        print e
        raise Http404


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def curso_eliminar(request):
    """
    Eliminar curso
    """
    try:
        codigo_curso = request.POST.get('codigo_curso')
        curso = Curso.objects.get(codigo=codigo_curso)
        curso.delete()
    except Exception as e:
        print e
        raise Http404

    return HttpResponseRedirect('/admin/cursos/listar/')


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def matricula_importar(request):
    """
    Importar matricula desde un archivo externo en formato .xlsx
    """
    mensaje = None
    mensaje_ok = None
    archivo_form = CargarArchivoForm()

    if request.method == "POST":
        archivo_form = CargarArchivoForm(request.POST, request.FILES)

        if archivo_form.is_valid():
            importar = importar_matricula_archivo(request.FILES['archivo'])

            if importar:
                mensaje_ok = 'Archivo importado correctamente'

            if not importar:
                mensaje = '''El archivo no contiene datos válidos o ya fueron
                importados anteriormente.'''

    data = {
        'formulario': archivo_form,
        'mensaje': mensaje,
        'mensaje_ok': mensaje_ok,
        'titulo_importar': 'matricula',
    }
    return render_to_response('archivo_importar.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def matricula_agregar(request):
    """
    Agregar nueva matrícula
    """
    mensaje_ok = mensaje_err = ''
    matricula_form = MatriculaForm

    if request.method == 'POST':
        matricula_form = MatriculaForm(request.POST)
        if matricula_form.is_valid():
            matricula_form.save()
            mensaje_ok = 'Datos guardados correctamente'
        else:
            mensaje_err = 'Error al guardar los datos'

    data = {
        'matricula_form': matricula_form,
        'mensaje_ok': mensaje_ok,
        'mensaje_err': mensaje_err
    }
    return render_to_response('matricula_agregar.html', data,
                              context_instance=RequestContext(request))


def buscar_matricula(filtro_curso, filtro_semestre):
    matriculas = Matricula.objects.filter(
        semestre=filtro_semestre,
        curso__codigo__icontains=filtro_curso).values(
            'semestre', 'curso', 'curso__nombre', 'docente'
        ).order_by('-semestre').annotate(Count('alumno'))

    return matriculas


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def matricula_listar(request):
    """
    Listar matrículas en el sistema
    """
    matriculas = Matricula.objects.values(
        'semestre', 'curso', 'curso__nombre', 'docente__nombres',
        'docente__apellido_paterno', 'docente__apellido_materno').order_by(
            '-semestre').annotate(
                Count('alumno'))
    semestres_form = SemestresForm()
    cuestionario_seleccionar_form = cuestionarioSeleccionarForm()

    # Filtrar cursos
    filtro_semestre = request.GET.get('semestre')
    filtro_curso = request.GET.get('curso')
    if filtro_semestre or filtro_curso:
        matriculas = buscar_matricula(filtro_curso, filtro_semestre)

    data = {
        'matriculas': matriculas,
        'semestres_form': semestres_form,
        'cuestionario_form': cuestionario_seleccionar_form
    }

    return render_to_response('matricula_listar.html', data,
                              context_instance=RequestContext(request))


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def matricula_eliminar(request):
    """
    Eliminar matrículas del sistema
    """
    try:
        curso = request.POST.get('matricula_codcurso')
        semestre = request.POST.get('matricula_semestre')
        matriculas = Matricula.objects.filter(curso=curso, semestre=semestre)

        for matricula in matriculas:
            matricula.delete()

    except Exception as e:
        print e
        raise Http404

    return HttpResponseRedirect('/admin/matricula/listar/')


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def matricula_eliminar_alumno(request):
    try:
        curso = request.POST.get('curso')
        semestre = request.POST.get('semestre')
        cod_alumno = request.POST.get('cod_alumno')
        matricula = Matricula.objects.get(
            curso__codigo=curso, semestre=semestre, alumno__codigo=cod_alumno)
        matricula.delete()

        return HttpResponseRedirect(
            '/admin/matricula/detalle/%s/%s/' % (semestre, curso))
    except Exception as e:
        print e
        raise Http404


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def matricula_detalle(request, semestre, codigo_curso):
    """
    Detalle de matrícula
    """
    curso = Curso.objects.get(codigo=codigo_curso)
    matriculas = Matricula.objects.filter(semestre=semestre, curso=curso)
    data = {
        'matriculas': matriculas,
        'semestre': semestre,
        'curso': curso
    }
    return render_to_response('matricula_detalles.html', data,
                              context_instance=RequestContext(request))


# == cuestionario
@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def cuestionario_crear(request, id_cuestionario=None):
    """
    Crear cuestionario nueva
    """
    # Cargar formulario de creación de cuestionario
    cuestionario_form = CuestionarioForm()
    cuestionario = None
    alternativas = None
    preguntas = categorias = []
    pregunta_form = PreguntaForm()
    categoria_form = CategoriaForm()
    alternativa_form = AlternativaForm()
    mensaje_enc_err = None
    mensaje = None

    if id_cuestionario:
        cuestionario = Cuestionario.objects.get(id=id_cuestionario)
        cuestionario_form = CuestionarioForm(instance=cuestionario)
        pregunta_form = PreguntaForm(cuestionario=cuestionario)
        # Listar categorías
        categorias = Categoria.objects.filter(cuestionario=cuestionario)
        # Listar preguntas
        preguntas = Pregunta.objects.filter(
            Q(categoria_id__in=[categoria.id for categoria in categorias]))
        # Listar alternativas
        alternativas = Alternativa.objects.filter(cuestionario=cuestionario)

    if request.method == 'POST':
        # Recuperar los datos llenados en el formulario
        cuestionario_form = CuestionarioForm(request.POST,
                                             instance=cuestionario)

        # Verificar si los datos llenados son válidos
        if cuestionario_form.is_valid():
            cuestionario = cuestionario_form.save(
                commit=False)  # Guardar el formulario
            if cuestionario.fecha_inicio <= cuestionario.fecha_final or not (
                    cuestionario.fecha_inicio and cuestionario.fecha_final):
                cuestionario.save()  # Guardar el cuestionario en la BD
                mensaje = 'Cuestionario guardado correctamente'
            else:
                mensaje_enc_err = 'Las fechas no son correctas'
                cuestionario = None

    data = {
        'mensaje_enc_err': mensaje_enc_err,
        'cuestionario_form': cuestionario_form,
        'cuestionario': cuestionario,
        'categorias': categorias,
        'preguntas': preguntas,
        'alternativas': alternativas,
        'pregunta_form': pregunta_form,
        'categoria_form': categoria_form,
        'alternativa_form': alternativa_form,
        'mensaje': mensaje,
    }

    return render_to_response(
        'cuestionario_crear.html', data,
        context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def cuestionario_ver(request):
    """
    Ver cuestionarios registradas
    """
    cuestionarios = Cuestionario.objects.all()
    data = {
        'cuestionarios': cuestionarios
    }
    return render_to_response('cuestionario_listar.html', data)


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def cuestionario_eliminar(request, id_cuestionario=None):
    """
    Eliminar cuestionario
    :param id_cuestionario: id de el cuestionario a eliminar
    """
    cuestionario = None
    try:
        cuestionario = Cuestionario.objects.get(id=id_cuestionario)
        cuestionario.delete()
    except cuestionario.DoesNotExist:
        print 'no cuestionario'
    return HttpResponseRedirect('/admin/cuestionario/ver/')


# -- Categorías
@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def categoria_agregar(request):
    """
    Agregar categoría a el cuestionario
    """
    if request.method == 'POST':
        # Recuperar los datos enviados por el formulario
        categoria_nombre = request.POST.get('nombre')
        id_cuestionario = request.POST.get('id_cuestionario')
        cuestionario = Cuestionario.objects.get(id=id_cuestionario)
        # Guardar los datos de la categoría
        categoria = Categoria()
        categoria.cuestionario = cuestionario
        categoria.nombre = categoria_nombre
        categoria.save()
        # Volver al formulario de creación de cuestionario
        return HttpResponseRedirect(
            '/admin/cuestionario/crear/%s/' % id_cuestionario)
        # Error
    raise Http404


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def categoria_editar(request, id_cuestionario, id_categoria):
    """
    Editar categoría
    :param id_categoria: ID de la categoría a editar
    """
    try:
        cuestionario = Categoria.objects.get(id=id_cuestionario)
        categoria = Categoria.objects.get(id=id_categoria)
        categoria_form = CategoriaForm(instance=categoria)

        if request.method == 'POST':
            categoria_form = CategoriaForm(request.POST, instance=categoria)
            if categoria_form.is_valid():
                categoria_form.save()
                return HttpResponseRedirect(
                    '/admin/cuestionario/crear/%s/' % cuestionario.id)

        data = {
            'categoria_form': categoria_form
        }
        return render_to_response('categoria_editar.html', data,
                                  context_instance=RequestContext(request))
    except Exception as e:
        print e
        raise Http404


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def categoria_eliminar(request):
    """
    Eliminar categoría de el cuestionario
    """
    if request.method == 'POST':
        # Recuperar los datos enviados por el formulario
        id_cuestionario = request.POST.get('id_cuestionario')
        id_categoria = request.POST.get('id_categoria')
        # Obtener la categoria de la BD
        categoria = Categoria.objects.get(id=id_categoria)
        # Eliminar la categoria
        categoria.delete()
        # Volver al formulario de creación de cuestionario
        return HttpResponseRedirect(
            '/admin/cuestionario/crear/%s/' % id_cuestionario)
        # Error
    raise Http404


# -- Preguntas
@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def pregunta_agregar(request):
    """
    Agregar pregunta al cuestionario
    """
    if request.method == 'POST':
        # Recuperar los datos enviados por el formulario
        id_cuestionario = request.POST.get('id_cuestionario')
        pregunta_nombre = request.POST.get('pregunta')
        categoria = request.POST.get('categoria')
        # Crear un objeto pregunta
        pregunta = Pregunta()
        pregunta.pregunta = pregunta_nombre
        pregunta.categoria_id = categoria
        # Guardar la pregunta en la BD
        pregunta.save()
        # Volver al formulario de creación de cuestionario
        return HttpResponseRedirect(
            '/admin/cuestionario/crear/%s/' % id_cuestionario)

    raise Http404


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def pregunta_eliminar(request):
    """
    Eliminar pregunta del cuestionario
    """
    if request.method == 'POST':
        # Recuperar los datos enviados por el formulario
        id_cuestionario = request.POST.get('id_cuestionario')
        id_pregunta = request.POST.get('id_pregunta')
        # recuperar la pregunta de la BD
        pregunta = Pregunta.objects.get(id=id_pregunta)
        # eliminar la pregunta
        pregunta.delete()
        # Volver al formulario de creación de cuestionario
        return HttpResponseRedirect(
            '/admin/cuestionario/crear/%s/' % id_cuestionario)

    raise Http404


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def pregunta_editar(request, id_cuestionario, id_pregunta):
    try:
        cuestionario = Cuestionario.objects.get(id=id_cuestionario)
        esta_pregunta = Pregunta.objects.get(id=id_pregunta)
        pregunta_form = PreguntaForm(cuestionario=cuestionario,
                                     instance=esta_pregunta)

        if request.method == 'POST':
            esta_pregunta = Pregunta.objects.get(id=id_pregunta)
            pregunta = request.POST.get('pregunta')
            categoria = request.POST.get('categoria')
            esta_pregunta.pregunta = pregunta
            esta_pregunta.categoria_id = categoria
            esta_pregunta.save()

            return HttpResponseRedirect(
                '/admin/cuestionario/crear/%s/' % id_cuestionario)
        data = {
            'pregunta_form': pregunta_form,
            'id_cuestionario': id_cuestionario,
            'pregunta': esta_pregunta
        }
        return render_to_response('pregunta_form.html', data,
                                  context_instance=RequestContext(request))

    except Exception as e:
        print e
        raise Http404


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def alternativa_agregar(request):
    """
    Agregar alternativa al cuestionario
    """
    if request.method == 'POST':
        # Recuperar los datos enviados por el formulario
        id_cuestionario = request.POST.get('id_cuestionario')
        alternativa_nombre = request.POST.get('alternativa')
        alternativa_valor = request.POST.get('valor')
        # Crear un objeto alternativa
        alternativa = Alternativa()
        alternativa.cuestionario_id = id_cuestionario
        alternativa.alternativa = alternativa_nombre
        alternativa.valor = alternativa_valor
        # Guardar la alternativa en la BD
        alternativa.save()
        # Volver al formulario de creación de cuestionario
        return HttpResponseRedirect(
            '/admin/cuestionario/crear/%s/' % id_cuestionario)

    raise Http404


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def alternativa_editar(request, id_cuestionario, id_alternativa):
    """
    Editar alternativa
    """
    alternativa = Alternativa.objects.get(id=id_alternativa)
    alternativa_form = AlternativaForm(instance=alternativa)
    if request.method == 'POST':
        alternativa_form = AlternativaForm(request.POST, instance=alternativa)
        if alternativa_form.is_valid():
            alternativa_form.save()
            return HttpResponseRedirect(
                '/admin/cuestionario/crear/%s/' % id_cuestionario)

    data = {
        'alternativa_form': alternativa_form
    }

    return render_to_response('alternativa_form.html', data,
                              context_instance=RequestContext(request))


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def alternativa_eliminar(request):
    """
    Eliminar alternativa del cuestionario
    """
    if request.method == 'POST':
        # Recuperar los datos enviados por el formulario
        id_cuestionario = request.POST.get('id_cuestionario')
        id_alternativa = request.POST.get('id_alternativa')
        # recuperar la pregunta de la BD
        alternativa = Alternativa.objects.get(id=id_alternativa)
        # eliminar la alternativa
        alternativa.delete()
        # Volver al formulario de creación de cuestionario
        return HttpResponseRedirect(
            '/admin/cuestionario/crear/%s/' % id_cuestionario)

    raise Http404


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def categoria_diapos(request, id_categoria, id_diapo=None):
    """
    Diapositivas para la categoría seleccionada
    """
    diapo = None
    if id_diapo:
        diapo = TextoAyuda.objects.get(id=id_diapo, tipo='D')
        # Mostrar formulario
    datos_iniciales = {'categoria': id_categoria, 'tipo': 'D'}
    formulario = TextoAyudaForm(initial=datos_iniciales)

    # Guardar nueva diapositiva
    if request.method == 'POST':
        formulario = TextoAyudaForm(request.POST, instance=diapo)
        if formulario.is_valid():
            formulario.save()

    # Mostrar formulario con diapositiva a editar
    if diapo:
        formulario = TextoAyudaForm(instance=diapo)

    # Listar diapositivas creadas
    diapositivas = TextoAyuda.objects.filter(categoria__id=id_categoria,
                                             tipo='D')
    data = {
        'formulario': formulario,
        'diapositivas': diapositivas,
        'categoria': Categoria.objects.get(id=id_categoria)
    }

    return render_to_response('categoria_diapos.html', data,
                              context_instance=RequestContext(request))


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def categoria_consejos(request, id_categoria):
    return render_to_response('categoria_consejos.html')


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def categoria_diapo_eliminar(request, id_categoria):
    if request.method == 'POST':
        id_diapo = request.POST.get('id_diapo')
        try:
            diapo = TextoAyuda.objects.get(id=id_diapo)
            diapo.delete()
        except Exception as e:
            print e

    return HttpResponseRedirect(
        '/admin/cuestionario/categoria/%s/diapos/' % id_categoria)


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def cuestionario_instrucciones(request, id_cuestionario):
    try:
        cuestionario = Cuestionario.objects.get(id=id_cuestionario)
        if request.method == 'POST':
            instrucciones = request.POST.get('instrucciones', '')
            cuestionario.instrucciones = instrucciones
            cuestionario.save()

            return HttpResponseRedirect(
                '/admin/cuestionario/crear/%d/' % cuestionario.id)

    except cuestionario.DoesNotExist:
        raise Http404


@user_passes_test(lambda u: u.groups.filter(name='administrador').count() == 1,
                  login_url='/')
def editar_passwd(request):
    if request.method == 'POST':
        mensaje_passwd_er = mensaje_passwd_ok = None
        # Recuperar el usuario que inició sesión

        # Recuperar contraseña anterior
        password = request.POST.get('passwd_ant')
        # Verificar si es la contraseña correcta
        if authenticate(username=request.user.username, password=password):
            # recuperar la nueva contraseña ingresada
            nuevo_passwd1 = request.POST.get('passwd_nueva1')
            nuevo_passwd2 = request.POST.get('passwd_nueva2')
            if nuevo_passwd1 == nuevo_passwd2:
                user = request.user
                user.set_password(nuevo_passwd1)
                user.save()
                mensaje_passwd_ok = u'Contraseña actualizada correctamente'
            else:
                mensaje_passwd_er = u'Las contraseñas ingresadas no coinciden'
        else:
            mensaje_passwd_er = u'La contraseña actual no es válida.'

        data = {
            'mensaje_passwd_er': mensaje_passwd_er,
            'mensaje_passwd_ok': mensaje_passwd_ok,
            'username': request.user.username,
            'plantilla': 'admin_base.html',
        }
        return render_to_response('cambia_password.html', data,
                                  context_instance=RequestContext(request))
    else:
        data = {
            'plantilla': 'admin_base.html'
        }
        return render_to_response('cambia_password.html', data,
                                  context_instance=RequestContext(request))


def show_403(request):
    """
    Mostrar pagina de error 403
    """
    return render_to_response('403.html')


def show_404(request):
    """
    Mostrar pagina de error 404
    """
    return render_to_response('404.html')


def admin_editar_passwd(request):
    pass