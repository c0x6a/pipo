# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.cuestionario.models import Cuestionario, Categoria, Pregunta, Alternativa

admin.site.register(Cuestionario)
admin.site.register(Categoria)
admin.site.register(Pregunta)
admin.site.register(Alternativa)

