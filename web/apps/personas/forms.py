# -*-  coding: utf-8 -*-
"""
Formulario para docentes
"""

from django.forms.models import ModelForm
from django.forms.widgets import TextInput
from apps.personas.models import Curso, Matricula


class CursoFormAdmin(ModelForm):
    """
    Formalario para editar cursos
    """

    class Meta:
        model = Curso
        widgets = {
            'codigo': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'nombre': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;'
                }
            )
        }


class CursoFormAgregar(ModelForm):
    class Meta:
        model = Curso
        widgets = {
            'codigo': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True,
                    'onKeyUp': "buscarCurso();"
                }
            ),
            'nombre': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True,
                }
            )
        }


class MatriculaForm(ModelForm):
    class Meta:
        model = Matricula
        widgets = {
            'alumno': TextInput(
                attrs={
                    'placeholder': u'Código de alumno',
                    'style': 'text-transform:uppercase;',
                    'required': True,
                }
            ),
            'curso': TextInput(
                attrs={
                    'placeholder': u'Código de curso',
                    'style': 'text-transform:uppercase;',
                    'required': True,
                }
            ),
            'docente': TextInput(
                attrs={
                    'placeholder': u'Código de docente',
                    'style': 'text-transform:uppercase;',
                    'required': True,
                }
            ),
            'semestre': TextInput(
                attrs={
                    'placeholder': u'Semestre de estudio',
                    'style': 'text-transform:uppercase;',
                    'required': True,
                }
            ),
        }