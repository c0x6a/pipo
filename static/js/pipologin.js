/**
 * Created with PyCharm.
 * User: carlosj2585
 * Date: 7/7/13
 * Time: 3:36 PM
 * To change this template use File | Settings | File Templates.
 */
function iniciar() {
    $('#formulario-login').removeClass('shake');
    $('#errorlogin').addClass('novisible');
    $('#validalogin').removeClass('novisible');
    var user = $('#id_user').val();
    var passwd = $('#id_password').val();
    Dajaxice.apps.pipologin.iniciar(Dajax.process, {'user': user, 'passwd': passwd});
}