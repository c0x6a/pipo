# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from django.forms.widgets import TextInput, DateInput, HiddenInput, Select
from django.forms.widgets import Textarea
from apps.cuestionario.models import Cuestionario, Pregunta, Categoria
from apps.cuestionario.models import TextoAyuda, Alternativa


class CuestionarioForm(ModelForm):
    """
    Formulario de cuestionario
    """

    class Meta:
        model = Cuestionario
        widgets = {
            'nombre': TextInput(
                attrs={
                    'placeholder': 'Nombre de la cuestionario'
                }
            ),
            'fecha_inicio': DateInput(
                attrs={
                    'placeholder': 'DD/MM/YYYY',
                    'autocomplete': 'off',
                }
            ),
            'fecha_final': DateInput(
                attrs={
                    'placeholder': 'DD/MM/YYYY',
                    'autocomplete': 'off'
                }
            ),
        }
        exclude = 'instrucciones'


class cuestionarioSeleccionarForm(forms.Form):
    """
    Seleccionar cuestionarios
    """
    cuestionario = forms.ModelChoiceField(queryset=Cuestionario.objects.all(),
                                          widget=Select(
                                              attrs={'required': True}))


class CategoriaForm(ModelForm):
    """
    Formulario de categorias
    """

    class Meta:
        model = Categoria
        widgets = {
            'cuestionario': HiddenInput(),
            'nombre': TextInput(
                attrs={
                    'required': '',
                }
            )
        }


class PreguntaForm(ModelForm):
    """
    Formulario de preguntas
    """

    def __init__(self, cuestionario=None, *args, **kwargs):
        super(PreguntaForm, self).__init__(*args, **kwargs)
        # Mostrar solo categorías del cuestionario seleccionado
        if self.instance:
            self.fields['categoria'].queryset = Categoria.objects.filter(
                cuestionario=cuestionario)

    class Meta:
        model = Pregunta
        widgets = {
            'pregunta': Textarea(
                attrs={
                    'required': True,
                    'style': 'width: 270px; height: 80px;'
                }
            ),
            'categoria': Select(
                attrs={
                    'required': True,
                }
            ),
        }
        exclude = 'id'


class AlternativaForm(ModelForm):
    class Meta:
        model = Alternativa
        widgets = {
            'cuestionario': HiddenInput(),
            'alternativa': TextInput(
                attrs={
                    'required': True,
                    'class': 'input-small',
                }
            ),
            'valor': TextInput(
                attrs={
                    'required': True,
                    'class': 'input-small',
                }
            )
        }


class TextoAyudaForm(ModelForm):
    # contenido = forms.CharField(widget=TinyMCE(
    #     attrs={'cols': 80, 'rows': 30}))

    class Meta:
        model = TextoAyuda
        widgets = {
            'categoria': HiddenInput(),
            'tipo': HiddenInput(),
        }