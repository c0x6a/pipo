# -*- coding: utf-8 -*-

# 3rd part imports
from django import forms
# local import
from apps.personas.models import Matricula


class CargarArchivoForm(forms.Form):
    """
    Formulario para cargar archivo
    """
    archivo = forms.FileField()


class SemestresForm(forms.Form):
    semestre = forms.ModelChoiceField(
        queryset=Matricula.objects.values_list('semestre',
                                               flat=True).distinct())