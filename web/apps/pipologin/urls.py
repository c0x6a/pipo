# -*- coding: UTF-8 -*-

# 3rd party imports
from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(
        r'^$',
        'apps.pipologin.views.login_user',
        name='iniciar_sesion'
    ),
    url(
        r'^logout/$',
        'apps.pipologin.views.logout_user',
        name='terminar_sesion'
    ),
    url(
        r'^redirect/$',
        'apps.pipologin.views.redirect_module',
        name='redireccionar'
    ),
)