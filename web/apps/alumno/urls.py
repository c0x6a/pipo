# -*- coding: UTF-8 -*-
"""
Urls de la aplicación cuestionario
"""

# 3rd party imports
from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^perfil/$', 'apps.alumno.views.alumno_editar_perfil'),
    url(r'^perfil/passwd/$', 'apps.alumno.views.alumno_editar_passwd'),
)