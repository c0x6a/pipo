# -*- coding: utf-8 -*-
"""
Modelos de la aplicación personas
"""
# System imports
# 3rd part imports
from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError
from django.db import models
# importaciones del proyecto
import re


def validar_letras(value):
    """
    Validar el ingreso de sólo letras
    """
    if not re.match(u'[a-zA-Z áéíóúÁÉÍÓÚñÑ.-]*', value):
        raise ValidationError(u'%s no es un valor válido' % value)


class CarreraProfesional(models.Model):
    """
    Clase Carrera Profesional
    """
    codigo = models.CharField(
        max_length=3, verbose_name=u'Código', primary_key=True)
    nombre = models.CharField(
        max_length=25, verbose_name='Carrera Profesional',
        validators=[validar_letras])

    def __unicode__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        """
        Sobreescribir el metodo save
        """
        # Si ya existe la carrera, no guardarla
        if CarreraProfesional.objects.filter(codigo=self.codigo).count():
            return self
        else:
            super(CarreraProfesional, self).save(*args, **kwargs)


class Curso(models.Model):
    """
    Clase Curso
    """
    codigo = models.CharField(
        max_length=10, verbose_name=u'Código', primary_key=True)
    nombre = models.CharField(
        max_length=80, verbose_name='Curso', validators=[validar_letras])

    def __unicode__(self):
        return self.nombre

    @property
    def cantidad_alumnos(self):
        """
        Ver la cantidad de alumnos que llevan este curso
        """
        alumnos = Matricula.objects.filter(curso=self).count()
        return alumnos

    def save(self):
        # Convertir a mayusculas
        self.codigo = self.codigo.upper()
        self.nombre = self.nombre.upper()
        # Guardar
        super(Curso, self).save()


class Alumno(models.Model):
    """
    Clase Alumno
    """
    SEXO_OPCIONES = (
        ('1', 'Masculino'),
        ('0', 'Femenino')
    )
    codigo = models.CharField(
        max_length=7, verbose_name=u'Código', primary_key=True)
    nombres = models.CharField(
        max_length=50, verbose_name='Nombre(s)', validators=[validar_letras])
    apellido_paterno = models.CharField(
        max_length=50, verbose_name='Apellido Paterno',
        validators=[validar_letras])
    apellido_materno = models.CharField(
        max_length=50, verbose_name='Apellido Materno',
        validators=[validar_letras])
    sexo = models.CharField(
        max_length=1, verbose_name='Sexo', choices=SEXO_OPCIONES)
    correo = models.EmailField(verbose_name='Correo', null=True, default='')
    carrera = models.ForeignKey(CarreraProfesional,
                                verbose_name=u'Código carrera')
    # carrera = models.CharField(max_length=3, verbose_name='Carrera')
    usuario = models.ForeignKey(User, blank=True, null=True)

    def __unicode__(self):
        return self.codigo

    def save(self):
        try:
            usuario = User.objects.get(username=self.codigo)
        except User.DoesNotExist:
            grupo = Group.objects.get(name='alumno')
            usuario = User.objects.create_user(self.codigo, self.correo,
                                               self.codigo)
            usuario.save()
            grupo.user_set.add(usuario)
            # Convertir a mayusculas
        self.nombres = self.nombres.upper()
        self.apellido_paterno = self.apellido_paterno.upper()
        self.apellido_materno = self.apellido_materno.upper()
        # Establecer usuario del sistema
        self.usuario = usuario
        super(Alumno, self).save()


class Docente(models.Model):
    """
    Clase Docente
    """
    codigo = models.CharField(
        max_length=5, verbose_name=u'Código', primary_key=True)
    nombres = models.CharField(
        max_length=50, verbose_name='Nombre(s)', validators=[validar_letras])
    apellido_paterno = models.CharField(
        max_length=50, verbose_name='Apellido Paterno',
        validators=[validar_letras])
    apellido_materno = models.CharField(
        max_length=50, verbose_name='Apellido Materno',
        validators=[validar_letras])
    correo = models.EmailField(verbose_name='Correo')
    usuario = models.ForeignKey(User, blank=True, null=True)

    def __unicode__(self):
        return self.codigo

    def save(self):
        try:
            usuario = User.objects.get(username=self.codigo)
        except User.DoesNotExist:
            grupo = Group.objects.get(name='docente')
            usuario = User.objects.create_user(self.codigo, self.correo,
                                               self.codigo)
            usuario.save()
            grupo.user_set.add(usuario)

        # Convertir a mayusculas
        self.nombres = self.nombres.upper()
        self.apellido_paterno = self.apellido_paterno.upper()
        self.apellido_materno = self.apellido_materno.upper()
        # Establecer usuario de sistema
        self.usuario = usuario
        # Guardar
        super(Docente, self).save()


class Matricula(models.Model):
    """
    Clase Matrícula
    """

    class Meta:
        unique_together = ('alumno', 'curso', 'docente', 'semestre')

    alumno = models.ForeignKey(Alumno)
    curso = models.ForeignKey(Curso)
    docente = models.ForeignKey(Docente)
    semestre = models.CharField(
        max_length=8, verbose_name='Semestre')

    def __unicode__(self):
        return str(self.id)
