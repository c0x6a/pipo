# -*- coding: utf-8 -*-
"""
Modelos de la aplicación «cuestionario»
"""
from django.db import models
from apps.personas.models import Alumno


class Cuestionario(models.Model):
    """
    Clase cuestionario
    """
    nombre = models.CharField(
        max_length=50, verbose_name='Nombre', unique=True)
    fecha_inicio = models.DateField(
        verbose_name='Fecha inicial', null=True, blank=True)
    fecha_final = models.DateField(
        verbose_name='Fecha Final', null=True, blank=True)
    instrucciones = models.TextField(
        verbose_name='Instrucciones', max_length=900, null=True, blank=True,
        default='')

    @property
    def cantidad_preguntas(self):
        """
        Ver la cantidad de preguntas de la cuestionario seleccionada
        """
        # Ver cantidad de categorias de las preguntas
        categorias = Categoria.objects.filter(cuestionario=self)
        # Sumar las preguntas de cada categoría
        cantidad_preguntas = sum(
            [categoria.cantidad_preguntas for categoria in categorias])

        return cantidad_preguntas

    @property
    def numero_categorias(self):
        """
        Ver el número de categorías registradas en la cuestionario
        """
        # Ver cantidad de categorias de las preguntas
        num_categorias = Categoria.objects.filter(cuestionario=self).count()

        return num_categorias

    def __unicode__(self):
        return self.nombre


class Categoria(models.Model):
    """
    Clase Categoría
    """
    cuestionario = models.ForeignKey(Cuestionario)
    nombre = models.CharField(max_length=20, verbose_name='Categoría')

    def __unicode__(self):
        return self.nombre

    @property
    def cantidad_preguntas(self):
        """
        Ver la cantidad de preguntas dentro de esta categoría
        """
        cant_preguntas = Pregunta.objects.filter(categoria=self).count()
        return cant_preguntas


class Pregunta(models.Model):
    """
    Clase Pregunta
    """
    pregunta = models.CharField(
        max_length=200, verbose_name='Pregunta')
    categoria = models.ForeignKey(Categoria)

    def __unicode__(self):
        return self.pregunta


class Alternativa(models.Model):
    """
    Clase Alternativa
    """
    cuestionario = models.ForeignKey(Cuestionario)
    alternativa = models.CharField(
        max_length=20, verbose_name='Alternativa')
    valor = models.IntegerField(verbose_name='Valor')

    def __unicode__(self):
        return self.alternativa


class cuestionarioAlumno(models.Model):
    """
    Clase cuestionario-Alumno
    """
    cuestionario = models.ForeignKey(Cuestionario)
    alumno = models.ForeignKey(Alumno)
    fecha = models.DateField()
    hora = models.TimeField()

    def __unicode__(self):
        return "%s-%s" % (self.cuestionario, self.alumno)


class cuestionarioAlumnoDetalles(models.Model):
    """
    Detalles para cuestionarioAlumno
    """
    cuestionario_alumno = models.ForeignKey(cuestionarioAlumno)
    pregunta = models.ForeignKey(Pregunta)
    respuesta = models.IntegerField()


class Resultado(models.Model):
    """
    Clase Resultado
    """
    cuestionario_llena = models.ForeignKey(cuestionarioAlumno)
    categoria = models.ForeignKey(Categoria)
    cantidad = models.FloatField()

    def __unicode__(self):
        return str(self.id)


class TextoAyuda(models.Model):
    """
    Clase Texto Ayuda, muestra consejos y/o diapositivas
    """
    TIPO_AYUDA = (
        ('D', 'Diapositiva'),
        ('C', 'Consejo'),
    )
    categoria = models.ForeignKey(Categoria)
    titulo = models.CharField(max_length=100, verbose_name=u'Título')
    contenido = models.TextField()
    tipo = models.CharField(choices=TIPO_AYUDA, max_length=1)