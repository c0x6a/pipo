# -*- coding: utf-8 -*-
"""
Vistas para la aplicacion Personas
"""
# 3rd part import
from django.shortcuts import render_to_response


def usuarios_home(request):
    return render_to_response('usuarios_home.html')