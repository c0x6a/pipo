/**
 * Created with PyCharm.
 * User: carlosj2585
 * Date: 7/7/13
 * Time: 5:43 PM
 */
function buscarAlumno() {
    var codigo = $('#id_codigo').val();
    if (codigo.length >= 6){
        Dajaxice.apps.pipoadmin.existe_alumno(Dajax.process, {'codigo': codigo});
    } else {
        $('#alumnoexiste').addClass('novisible');
        $('#alumnoexiste').removeClass('fadeInUp');
    }
};

function buscarDocente() {
    var codigo = $('#id_codigo').val();
    if (codigo.length >= 5){
        Dajaxice.apps.pipoadmin.existe_docente(Dajax.process, {'codigo': codigo});
    } else {
        $('#docenteexiste').addClass('novisible');
        $('#docenteexiste').removeClass('fadeInUp');
    }
};

function buscarCurso() {
    var codigo = $('#id_codigo').val();
    if (codigo.length >= 6){
        Dajaxice.apps.pipoadmin.existe_curso(Dajax.process, {'codigo': codigo});
    } else {
        $('#cursoexiste').addClass('novisible');
        $('#cursoexiste').removeClass('fadeInUp');
    }
};