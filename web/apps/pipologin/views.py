# -*- coding: utf-8 -*-
"""
Vistas de logueo de usuarios
"""

# 3rd part imports
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
# local app imports
from apps.pipologin.forms import LoginForm


def login_user(request):
    """
    Loguear usuario en el sistema
    """
    if request.user and request.user.is_active:
        return redirect_module(request)
    else:
        # Cargar formulario para iniciar sesión
        login_form = LoginForm()
        # Variables necesarias
        mensaje_error = ''
        url_next = request.GET.get('next')
        if url_next:
            request.session['url_next'] = url_next

        if request.method == 'POST':
            login_form = LoginForm(request.POST)

            if login_form.is_valid():
                username = request.POST.get('user')
                password = request.POST.get('password')
                user = authenticate(username=username, password=password)

                if user and user.is_active:
                    login(request, user)
                    try:
                        return HttpResponseRedirect(request.session['url_next'])
                    except Exception as e:
                        print e
                        return redirect_module(request)

            mensaje_error = 'Datos incorrectos de inicio de sesión.'

        data = {
            'login_form': login_form,
            'mensaje_error': mensaje_error
        }

        return render_to_response(
            'login_form.html', data, context_instance=RequestContext(request))


def logout_user(request):
    """
    Desloguear usuario del sistema
    """
    logout(request)
    # Destuir una session
    return HttpResponseRedirect('/')


def redirect_module(request):
    """
    Redirigir al modulo correspondiente del usuario
    """
    try:
        group = request.user.groups.values_list('name', flat=True)[0]
    except Exception as e:
        print e.message
        group = None

    # Ver si es la primera vez que el usuario inicia sesión, por defecto la
    # contraseña es el mismo nombre de usuario, si es así, redireccionar a la
    # ventana de cambiar contraseña
    usuario = request.user.username
    if usuario != 'admin' and authenticate(username=usuario, password=usuario):
        return HttpResponseRedirect('%s/perfil/passwd/' % group)

    if group == 'alumno':
        url = '/cuestionario/'
    elif group == 'docente':
        url = '/docente/'
    elif group == 'administrador':
        url = '/admin/'
    else:
        url = '/403/'

    return HttpResponseRedirect(url)
